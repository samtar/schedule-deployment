# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import datetime
import logging

import mwclient
import mwparserfromhell

from . import settings
from . import utils

logger = logging.getLogger(__name__)

EVENT_TEMPLATE = "Deployment calendar event card"
BACKPORT_MARKER = "[[Backport windows"
INSTRUCTIONS_NICK = "irc-nickname"


def parse_wikitext(wikitext):
    return mwparserfromhell.parse(wikitext)


def extract_when(template):
    return str(template.get("when").value).strip()


def extract_window_name(template):
    window = parse_wikitext(template.get("window").value)
    link = window.filter_wikilinks()[0]
    return str(link.text)


def find_all_backport_windows(wikitext):
    """Extract a list of backport windows from a blob of wikitext."""
    windows = []
    wikicode = parse_wikitext(wikitext)
    for template in wikicode.ifilter_templates():
        if template.name.matches(EVENT_TEMPLATE):
            if template.has("window") and template.get(
                "window",
            ).value.startswith(BACKPORT_MARKER):
                when_str = extract_when(template)
                when = utils.window_when_to_datetime(when_str)
                name = when.strftime("%A, %B %d ")
                name += extract_window_name(template)
                windows.append((when_str, name, when))
    return windows


def find_window_template(parsed, when):
    for template in parsed.ifilter_templates():
        if template.name.matches(EVENT_TEMPLATE):
            if extract_when(template) == when:
                return template
    return None


def get_templates_by_name(node, name):
    return [
        tmpl
        for tmpl in node.ifilter_templates(recursive=False)
        if tmpl.name.matches(name)
    ]


def add_item_to_window(window, nick, name, item):
    logger.debug("Before: %s", window)
    marker = utils.build_ircnick(nick, name)
    search_nick = nick.lower()
    what = window.get("what").value
    insert_next = False
    for section in get_templates_by_name(what, "ircnick"):
        section_nick = section.params[0].lower()
        logger.debug("section: %s; nick: %s", section, section_nick)
        if insert_next or section_nick == INSTRUCTIONS_NICK:
            if not insert_next:
                # Create a new section
                what.insert_before(section, marker + "\n", recursive=False)
            what.insert_before(section, item + "\n", recursive=False)
        if section_nick == search_nick:
            # Found existing section for our user
            # We now want to insert before the next item.
            insert_next = True
    logger.debug("After: %s", window)


class Client:

    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get a MediaWiki client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.MEDIAWIKI_HOST,
                settings.MEDIAWIKI_CONSUMER_TOKEN,
                settings.MEDIAWIKI_CONSUMER_SECRET,
                settings.MEDIAWIKI_ACCESS_TOKEN,
                settings.MEDIAWIKI_ACCESS_SECRET,
                settings.MEDIAWIKI_DEPLOYMENTS_PAGE,
            )
        return cls._default_instance

    def __init__(
        self,
        host,
        consumer_token,
        consumer_secret,
        access_token,
        access_secret,
        deploy_page,
    ):
        self.mwsite = mwclient.Site(
            host,
            consumer_token=consumer_token,
            consumer_secret=consumer_secret,
            access_token=access_token,
            access_secret=access_secret,
            clients_useragent=utils.USER_AGENT,
        )
        self.deploy_page = deploy_page

    def get_upcoming_windows(self):
        now = datetime.datetime.now(datetime.UTC)
        page = self.mwsite.Pages[self.deploy_page]
        windows = find_all_backport_windows(page.text())
        # (value, label, render_kw{})
        return [
            (
                window[0],
                window[1],
                {
                    "data-epoch": int(window[2].timestamp() * 1000),
                },
            )
            for window in windows
            if window[2] > now
        ]

    def update_window(self, form, change):
        when = form.window.data
        nick = form.irc_nick.data
        name = form.name.data
        item = utils.build_backport_item(form, change)
        summary = f"Add [[gerrit:{change['_number']}]] to {when} window"

        page = self.mwsite.Pages[self.deploy_page]

        parsed = parse_wikitext(page.text())
        window = find_window_template(parsed, when)
        add_item_to_window(window, nick, name, item)

        page.save(str(parsed), summary=summary)
