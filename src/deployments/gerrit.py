# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import json
import logging

import requests

from . import settings
from . import utils

logger = logging.getLogger(__name__)


class APIError(Exception):
    def __init__(self, message, code, result):
        self.message = message
        self.code = code
        self.result = result

    def __str__(self):
        return f"{self.message} ({self.code})"


class RESTClient:
    _default_instance = None

    @classmethod
    def default_client(cls):
        """Get a Gerrit client using the default credentials."""
        if cls._default_instance is None:
            logger.debug("Creating default instance")
            cls._default_instance = cls(
                settings.GERRIT_URL,
            )
        return cls._default_instance

    def __init__(self, url):
        self.url = url
        self.session = requests.Session()
        self.headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Accept-Encoding": "gzip",
            "User-Agent": utils.USER_AGENT,
        }

    def _request(self, verb, path, payload=None, params=None):
        url = f"{self.url}/{path}"
        r = self.session.request(
            method=verb,
            url=url,
            headers=self.headers,
            params=params,
            json=payload,
            timeout=(1, 5),
        )
        if r.status_code > 299:
            raise APIError(
                f"Got response {r.status_code} for {url}: {r.content}",
                r.status_code,
                r.text,
            )
        # Remove the weird anti-XSSI prefix string from the response
        body = r.text.lstrip(")]}'")
        logger.debug("%s %s: %s", verb, path, r)
        return json.loads(body)

    def get(self, path, params=None):
        return self._request("GET", path, params=params)

    def get_change(self, change_id):
        return self.get(
            f"changes/{change_id}",
            params=[
                ("o", "CURRENT_REVISION"),
                ("o", "COMMIT_FOOTERS"),
                ("o", "TRACKING_IDS"),
            ],
        )
