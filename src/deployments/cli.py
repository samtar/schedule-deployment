# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import logging
import logging.handlers

import click
import coloredlogs

from .logging import PrivateTimedRotatingFileHandler
from .version import __version__
from .web import main as web_main

logger = logging.getLogger(__name__)


@click.group()
@click.version_option(version=__version__)
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase debug logging verbosity",
)
@click.option(
    "--logfile",
    default=None,
    type=click.Path(dir_okay=False),
    help="Log to this (rotated) log file",
)
@click.pass_context
def main(ctx, verbose, logfile):
    """Add a patch to a backport window on the Deployments calendar."""
    ctx.ensure_object(dict)
    ctx.obj["VERBOSE"] = verbose
    ctx.obj["LOGFILE"] = logfile

    log_fmt = "%(asctime)s %(name)s %(levelname)s: %(message)s"
    log_datefmt = "%Y-%m-%dT%H:%M:%SZ"
    coloredlogs.install(
        level=max(logging.DEBUG, logging.WARNING - (10 * verbose)),
        fmt=log_fmt,
        datefmt=log_datefmt,
        level_styles=coloredlogs.DEFAULT_LEVEL_STYLES
        | {
            "debug": {},
            "info": {"color": "green"},
        },
        field_styles=coloredlogs.DEFAULT_FIELD_STYLES
        | {
            "asctime": {"color": "yellow"},
        },
    )
    logging.captureWarnings(True)

    if logfile:
        handler = PrivateTimedRotatingFileHandler(
            logfile,
            when="midnight",
            backupCount=7,
            utc=True,
            encoding="utf-8",
        )
        handler.setLevel(logging.INFO)
        if verbose >= 3:
            # Don't send debug level to disk until `-vvv`
            handler.setLevel(logging.DEBUG)
        handler.setFormatter(
            logging.Formatter(fmt=log_fmt, datefmt=log_datefmt),
        )
        logging.getLogger().addHandler(handler)
        click.echo(f"Logging to {logfile} with daily rotation.")

    logger.debug("Invoking %s", ctx.invoked_subcommand)


@main.command()
@click.pass_context
def web(ctx):
    """Multi-purpose web service."""
    ctx.exit(web_main())


if __name__ == "__main__":  # pragma: nocover
    main()
