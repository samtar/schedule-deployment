# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import logging
import logging.handlers
import os

import gunicorn.glogging


def private_open(file, flags, dir_fd=None):
    """Open a file with ``u=rw,g=,o=`` permissions."""
    return os.open(file, flags, mode=0o600, dir_fd=dir_fd)


class PrivateTimedRotatingFileHandler(
    logging.handlers.TimedRotatingFileHandler,
):
    """Rotate log files at timed intervals with ``u=rw,g=,o=`` permissions."""

    def _open(self):
        return open(
            self.baseFilename,
            self.mode,
            encoding=self.encoding,
            opener=private_open,
        )


class GunicornLogger(gunicorn.glogging.Logger):

    def __init__(self, cfg):
        self.error_log = logging.getLogger("gunicorn.error")
        self.access_log = logging.getLogger("gunicorn.access")
        self.cfg = cfg
        self.loglevel = logging.root.level

    def setup(self, cfg):  # noqa: U100
        pass

    def access(self, resp, req, environ, request_time):
        if environ["RAW_URI"] == "/healthz":
            # Ignore k8s /healthz pings
            return

        if self.access_log is not None:
            safe_atoms = self.atoms_wrapper_class(
                self.atoms(resp, req, environ, request_time),
            )
            self.access_log.debug(self.cfg.access_log_format, safe_atoms)
