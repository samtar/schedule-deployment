# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import dataclasses
import datetime
import re

import pytz
import requests

from . import settings

USER_AGENT = "{name} ({url}; {email}) python-requests/{vers}".format(
    name="schedule-deployment",
    url="https://wikitech.wikimedia.org/wiki/Tool:Schedule-deployment",
    email=f"{settings.TOOL_NAME}.maintainers@toolforge.org",
    vers=requests.__version__,
)

TIMEZONES = {
    "DE": pytz.timezone("Europe/Berlin"),
    "SF": pytz.timezone("America/Los_Angeles"),
    "UTC": pytz.utc,
    "Z": pytz.utc,
}

RE_WHEN = re.compile(
    r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}) "
    r"(?P<hour>\d{2}):(?P<minute>\d{2}) "
    r"(?P<tz>[A-Z]{2})",
)


def window_when_to_datetime(when):
    """Convert a 'when' string into a UTC datetime."""
    m = RE_WHEN.match(when)
    if m:
        tz = TIMEZONES.get(m.group("tz"), "UTC")
        local_dt = tz.localize(
            datetime.datetime(
                year=int(m.group("year")),
                month=int(m.group("month")),
                day=int(m.group("day")),
                hour=int(m.group("hour")),
                minute=int(m.group("minute")),
            ),
        )
        return local_dt.astimezone(pytz.utc)
    return datetime.datetime.fromtimestamp(0, datetime.UTC)


@dataclasses.dataclass
class Status:
    ok: bool = True
    message: str = None

    def set_error(self, msg):
        self.ok = False
        self.message = msg


def validate_backport(change):
    valid = Status()
    project = change["project"]
    branch = change["branch"]
    status = change["status"]

    if project == "operations/mediawiki-config":
        if branch != "master":
            valid.set_error(
                "MediaWiki config patches must be on the master branch.",
            )
    elif (
        project == "mediawiki/core"
        or project.startswith("mediawiki/extensions/")
        or project.startswith("mediawiki/skins/")
    ):
        if not branch.startswith("wmf/"):
            valid.set_error(
                f"Patches on the {branch} branch of a MediaWiki repo cannot "
                "be backported. Cherry-pick to a <code>wmf/</code> release "
                "branch and try again.",
            )
    else:
        valid.set_error("Backport windows are only for MediaWiki repos.")

    if valid.ok and status != "NEW":
        valid.set_error("Only unmerged changes can be backported.")

    return valid


def get_change_type(change):
    if change["project"] == "operations/mediawiki-config":
        return "config"
    return change["branch"].split("/")[1]


def get_bugs(change):
    bugs = [
        "{{phabricator|%s}}" % bug["id"]
        for bug in change.get("tracking_ids", [])
        if bug["system"] == "Phab"
    ]
    if bugs:
        return "- " + " ".join(bugs)
    return ""


def build_backport_item(form, change):
    gid = change["_number"]
    subject = form.description.data or change["subject"]
    ctype = get_change_type(change)
    bugs = get_bugs(change)
    return f"* [{ctype}] {{{{gerrit|{gid}}}}} {subject} {bugs}"


def build_ircnick(nick, name=None):
    if not name:
        name = nick
    return f"{{{{ircnick|{nick}|{name}}}}}"
