# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import pathlib

import environ

env = environ.Env()
env.smart_cast = False
environ.Env.read_env(env_file=pathlib.Path.cwd() / ".env")

PACKAGE_PATH = pathlib.Path(__file__).parent

# == Web settings ==
WEB_BIND = env.list("WEB_BIND", default=["0.0.0.0:8000"])
WEB_WORKERS = env.int("WEB_WORKERS", default=3)

# == tool settings ==
TOOL_NAME = env.str("TOOL_NAME", default="schedule-deployment")

# == Gerrit settings ==
GERRIT_URL = env.str("GERRIT_URL", default="https://gerrit.wikimedia.org/r")

# == MediaWik settings ==
MEDIAWIKI_HOST = env.str("MEDIAWIKI_HOST", default="wikitech.wikimedia.org")
MEDIAWIKI_CONSUMER_TOKEN = env.str("MEDIAWIKI_CONSUMER_TOKEN")
MEDIAWIKI_CONSUMER_SECRET = env.str("MEDIAWIKI_CONSUMER_SECRET")
MEDIAWIKI_ACCESS_TOKEN = env.str("MEDIAWIKI_ACCESS_TOKEN")
MEDIAWIKI_ACCESS_SECRET = env.str("MEDIAWIKI_ACCESS_SECRET")
MEDIAWIKI_DEPLOYMENTS_PAGE = env.str(
    "MEDIAWIKI_DEPLOYMENTS_PAGE",
    default="Deployments",
)
