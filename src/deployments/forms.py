# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import logging

import flask
import flask_wtf
from wtforms.fields import IntegerField
from wtforms.fields import SelectField
from wtforms.fields import StringField
from wtforms.fields import SubmitField
from wtforms.validators import DataRequired

from . import mediawiki
from . import settings

COOKIE_IRC_NICK = "irc_nick"
COOKIE_NAME = "name"

logger = logging.getLogger(__name__)


def get_nick_from_cookie():
    return flask.request.cookies.get(COOKIE_IRC_NICK)


def get_name_from_cookie():
    name = flask.request.cookies.get(COOKIE_NAME)
    if not name:
        return get_nick_from_cookie()
    return name


def get_backport_windows():
    if settings.MEDIAWIKI_CONSUMER_TOKEN == "DUMMY_VALUE_FOR_TESTS":
        # HACK: keep things from blowing up in CI
        return []
    mw = mediawiki.Client.default_client()
    return mw.get_upcoming_windows()


class GerritIdForm(flask_wtf.FlaskForm):
    gerrit_id = IntegerField(
        label="Gerrit change number",
        validators=[DataRequired()],
        description="like 677325",
    )
    submit = SubmitField("Schedule")


class BackportForm(flask_wtf.FlaskForm):
    irc_nick = StringField(
        label="Your IRC nick",
        validators=[DataRequired()],
        default=get_nick_from_cookie,
    )
    name = StringField(
        label="Your display name",
        default=get_name_from_cookie,
    )
    description = StringField(
        label="Description",
    )
    window = SelectField(
        label="Backport window",
        choices=get_backport_windows(),
    )
    submit = SubmitField("Schedule deployment")
