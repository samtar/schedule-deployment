# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import deployments.mediawiki

from . import root

data_path = root / "tests" / "data" / "mediawiki"


def test_find_all_backport_windows():
    with (data_path / "deployments.wikitext").open(encoding="utf-8") as f:
        page = f.read()
    windows = deployments.mediawiki.find_all_backport_windows(page)
    assert len(windows) == 23
    assert [window[0] for window in windows] == [
        "2024-05-27 00:00 SF",
        "2024-05-27 06:00 SF",
        "2024-05-28 00:00 SF",
        "2024-05-28 06:00 SF",
        "2024-05-28 13:00 SF",
        "2024-05-29 00:00 SF",
        "2024-05-29 06:00 SF",
        "2024-05-29 13:00 SF",
        "2024-05-30 00:00 SF",
        "2024-05-30 06:00 SF",
        "2024-05-30 13:00 SF",
        "2024-06-03 00:00 SF",
        "2024-06-03 06:00 SF",
        "2024-06-03 13:00 SF",
        "2024-06-04 00:00 SF",
        "2024-06-04 06:00 SF",
        "2024-06-04 13:00 SF",
        "2024-06-05 00:00 SF",
        "2024-06-05 06:00 SF",
        "2024-06-05 13:00 SF",
        "2024-06-06 00:00 SF",
        "2024-06-06 06:00 SF",
        "2024-06-06 13:00 SF",
    ]
    assert windows[0][1] == "Monday, May 27 UTC morning backport window"


def test_find_window_template():
    with (data_path / "deployments.wikitext").open(encoding="utf-8") as f:
        page = f.read()
    parsed = deployments.mediawiki.parse_wikitext(page)
    when = "2024-06-05 06:00 SF"
    window = deployments.mediawiki.find_window_template(parsed, when)
    assert deployments.mediawiki.extract_when(window) == when


def get_window_for_insert_tests():
    with (data_path / "deployments.wikitext").open(encoding="utf-8") as f:
        page = f.read()
    parsed = deployments.mediawiki.parse_wikitext(page)
    when = "2024-05-27 06:00 SF"
    return deployments.mediawiki.find_window_template(parsed, when)


def test_get_templates_by_name():
    window = get_window_for_insert_tests()
    what = window.get("what").value
    ircnicks = deployments.mediawiki.get_templates_by_name(what, "ircnick")
    assert len(ircnicks) == 5
    assert ircnicks[-1] == "{{ircnick|irc-nickname|Requesting Developer}}"


def test_add_item_to_window():
    window = get_window_for_insert_tests()
    deployments.mediawiki.add_item_to_window(
        window,
        "bd808",
        "BryanDavis",
        "* TEST INSERT",
    )
    assert window.endswith(
        "{{ircnick|bd808|BryanDavis}}\n"
        "* TEST INSERT\n"
        "{{ircnick|irc-nickname|Requesting Developer}}\n"
        "* ''Gerrit link to backport or config change''\n"
        "}}",
    )
