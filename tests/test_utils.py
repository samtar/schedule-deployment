# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikimedia Deployment Scheduler
#
# Wikimedia Deployment Scheduler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Wikimedia Deployment Scheduler is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikimedia Deployment Scheduler. If not, see <http://www.gnu.org/licenses/>.
import json

import deployments.forms
import deployments.utils

from . import root

data_path = root / "tests" / "data" / "utils"


def test_window_when_to_datetime():
    test_cases = {
        "": "1970-01-01T00:00:00+00:00",
        "2024-05-27 00:00 SF": "2024-05-27T07:00:00+00:00",
        "2024-05-27 09:00 DE": "2024-05-27T07:00:00+00:00",
        "2024-05-27 03:00 SF": "2024-05-27T10:00:00+00:00",
        "2024-05-27 12:00 DE": "2024-05-27T10:00:00+00:00",
    }
    for given, expect in test_cases.items():
        dt = deployments.utils.window_when_to_datetime(given)
        assert dt.isoformat() == expect, given


def test_validate_backport():
    test_cases = {
        "bad-repo.json": False,
        "mediawiki-bad-branch.json": False,
        "mediawiki-merged.json": False,
        "mediawiki-ok.json": True,
        "skin-bad-branch.json": False,
        "wmf-config-bad-branch.json": False,
        "wmf-config-ok.json": True,
    }

    for fname, expect in test_cases.items():
        with (data_path / fname).open(encoding="utf-8") as f:
            change = json.load(f)
        assert deployments.utils.validate_backport(change).ok == expect, fname


def test_get_change_type():
    ctype = deployments.utils.get_change_type(
        {"project": "operations/mediawiki-config"},
    )
    assert ctype == "config", "operations/mediawiki-config"

    ctype = deployments.utils.get_change_type(
        {"project": "", "branch": "wmf/1.42.0-wmf.16"},
    )
    assert ctype == "1.42.0-wmf.16", "wmf/1.42.0-wmf.16"


def test_get_bugs():
    assert deployments.utils.get_bugs({}) == "", "empty"
    fixture = {"tracking_ids": [{"system": "Phab", "id": "T12345"}]}
    assert deployments.utils.get_bugs(fixture) == "- {{phabricator|T12345}}"


def test_build_backport_item(app):
    @app.post("/")
    def index():
        form = deployments.forms.BackportForm()
        with (data_path / "mediawiki-ok.json").open(encoding="utf-8") as f:
            change = json.load(f)
        expect = (
            "* [1.42.0-wmf.16] {{gerrit|997279}} "
            "Use decodeURI for comment ID searches as well as heading searches "
            "- {{phabricator|T356199}}"
        )
        assert deployments.utils.build_backport_item(form, change) == expect

    app.test_client().post("/", data={"irc_nick": "bd808"})


def test_build_ircnick():
    build_ircnick = deployments.utils.build_ircnick
    assert build_ircnick("bd808") == "{{ircnick|bd808|bd808}}"
    assert (
        build_ircnick("bd808", "BryanDavis") == "{{ircnick|bd808|BryanDavis}}"
    )
